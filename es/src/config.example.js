module.exports = {
    apiNode: "wss://node.dxperts.asia",
    useES: true,
    // use elastic search
    esNode: "https://wrapper.elasticsearch.dxperts.ws",
    botPaymentAccounts: []
};
